const express = require('express')
const bodyParser = require('body-parser')
const models = require("./models");
const controllers = require("./controllers");
const app = express()
const port = 3000
const authenticator = require('./middleman/authenticator');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' })
})

app.get('/posts', controllers.posts.list);
app.get('/posts/:id', controllers.posts.find);
app.post('/posts', controllers.posts.create);
app.put('/posts/:id', controllers.posts.update);
app.delete('/posts/:id', controllers.posts.delete);

app.get('/users', controllers.users.list);
app.get('/users/:id', controllers.users.find);
app.post('/users', controllers.users.create);
app.put('/users/:id', controllers.users.update);
app.delete('/users/:id', controllers.users.delete);

app.use('/reactions*', authenticator);
app.get('/reactions', controllers.reactions.list);
app.get('/reactions/:id', controllers.reactions.find);
app.post('/reactions', controllers.reactions.create);
app.put('/reactions/:id', controllers.reactions.update);
app.delete('/reactions/:id', controllers.reactions.delete);

models.sequelize.sync().then(function () {
  if (process.env.NODE_ENV == 'test') { return true; }

  app.listen(port, () => {
    console.log(`App running on port ${port}.`)
  })
});

module.exports = app

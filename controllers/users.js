const User = require('../models').User;

module.exports = {
    list(req, res) {
        return User.findAll().then((result) => res.json(result))
    },

    find(req, res) {
        return User.findByPk(req.params.id)
        .then((user) => {
            if (!user) {
                return res.status(404).send({
                    message: 'User Not Found'
                });
            }
            return res.send(user);
        })
    },

    create(req, res) {
        return User.create({
            email: req.body.email,
            password: req.body.password
        })
        .then((result) => res.status(201).json(result))
        .catch((error) => res.status(400).send(error.error))
    },

    update(req, res) {
        return User.findByPk(req.params.id)
        .then(user => {
            if (!user) {
                return res.status(404).send({
                    message: 'User Not Found'
                });
            }

            return user
            .update({
                email: req.body.email,
                password: req.body.password
            })
            .then(() => res.send(user))
            .catch((error) => res.status(400).send(error.errors));
        })
        .catch((error) => res.status(400).send(error))
    },

    delete(req, res) {
        return User.findByPk(req.params.id)
        .then(user => {
            if (!user) {
                return res.status(400).send({
                    message: 'User Not Found'
                });
            }
            return user
            .destroy()
            .then(() => res.status(204).send())
            .catch((error) => res.status(400).send(error.errors));
        })
        .catch((error) => res.status(400).send(error))
    },
}

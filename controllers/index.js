const posts = require('./posts');
const users = require('./users');
const reactions = require('./reactions');

module.exports = {
    posts,
    users,
    reactions,
};

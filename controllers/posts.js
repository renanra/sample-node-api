const Post = require('../models').Post;

module.exports = {
    list(req, res) {
        return Post.findAll().then((result) => res.json(result))
    },

    find(req, res) {
        return Post.findByPk(req.params.id)
        .then((post) => {
            if (!post) {
                return res.status(404).send({
                    message: 'Post Not Found'
                });
            }
            return res.send(post);
        })
        .catch((error) => res.status(400).send(error))
    },

    create(req, res) {
        return Post.create({
            title: req.body.title,
            content: req.body.content
        })
        .then((result) => res.status(201).json(result))
        .catch((error) => res.status(400).send(error.errors))
    },

    update(req, res) {
        return Post.findByPk(req.params.id)
        .then(post => {
            if (!post) {
                return res.status(404).send({
                    message: 'Post Not Found'
                });
            }

            return post
            .update({
                title: req.body.title,
                content: req.body.content
            })
            .then(() => res.send(post))
            .catch((error) => res.status(400).send(error));
        })
        .catch((error) => res.status(400).send(error))
    },

    delete(req, res) {
        return Post.findByPk(req.params.id)
        .then(post => {
            if (!post) {
                return res.status(400).send({
                    message: 'Post Not Found'
                });
            }
            return post
            .destroy()
            .then(() => res.status(204).send())
            .catch((error) => res.status(400).send(error));
        })
        .catch((error) => res.status(400).send(error))
    },
}

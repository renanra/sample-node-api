const Reaction = require('../models').Reaction;

module.exports = {
    list(req, res) {
        return Reaction
        .findAll({ where: { UserId: req.currentUser.id } })
        .then((result) => res.json(result))
    },

    find(req, res) {
        return Reaction.findOne({ where: { id: req.params.id, UserId: req.currentUser.id }})
        .then((reaction) => {
            if (!reaction) {
                return res.status(404).send({
                    message: 'Reaction Not Found'
                });
            }
            return res.send(reaction);
        })
        .catch((error) => res.status(400).send(error))
    },

    create(req, res) {
        return Reaction.create({
            name: req.body.name,
            PostId: req.body.PostId,
            UserId: req.currentUser.id,
        })
        .then((result) => res.status(201).json(result))
        .catch((error) => res.status(400).send(error.errors))
    },

    update(req, res) {
        return Reaction.findOne({ where: { id: req.params.id, UserId: req.currentUser.id }})
        .then(reaction => {
            if (!reaction) {
                return res.status(404).send({
                    message: 'Reaction Not Found'
                });
            }

            return reaction
            .update({
                name: req.body.name,
                PostId: req.body.PostId
            })
            .then(() => res.send(reaction))
            .catch((error) => res.status(400).send(error.errors));
        })
        .catch((error) => res.status(400).send(error))
    },

    delete(req, res) {
        return Reaction.findOne({ where: { id: req.params.id, UserId: req.currentUser.id }})
        .then(reaction => {
            if (!reaction) {
                return res.status(400).send({
                    message: 'Reaction Not Found'
                });
            }
            return reaction
            .destroy()
            .then(() => res.status(204).send())
            .catch((error) => res.status(400).send(error.errors));
        })
        .catch((error) => res.status(400).send(error))
    },
}

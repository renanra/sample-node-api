process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();
const User = require('../models').User;

chai.use(chaiHttp);

describe('Users', () => {
    let attributes = {
        email: "user@example.com",
        password: "123456",
    };

    beforeEach((done) => {
        User.destroy({ truncate: { cascade: true } }).then(() => done());
    });

    describe('GET /users', () => {
        it('it should GET all the users', (done) => {
            chai.request(server)
            .get('/users')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(0);
                done();
            });
        });
    });

    describe('POST /users', () => {
        it('it should POST a user ', (done) => {
            chai.request(server)
            .post('/users')
            .send(attributes)
            .end((err, res) => {
                res.should.have.status(201);
                res.body.should.be.a('object');
                res.body.should.have.property('email');
                res.body.should.have.property('password');
                done();
            });
        });
    });

    describe('GET /users/:id', () => {
        it('it should GET a user by the given id', (done) => {
            User.create(attributes).then((user) => {
                chai.request(server)
                .get('/users/' + user.id)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('email');
                    res.body.should.have.property('password');
                    done();
                });
            }).catch((err) => { console.log(err) });

        });
    });

    describe('PUT /users/:id', () => {
        it('it should UPDATE a user given the id', (done) => {
            User.create(attributes).then((user) => {
                chai.request(server)
                .put('/users/' + user.id)
                .send({ email: "updated@example.com" })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('email').eql('updated@example.com');
                    done();
                });
            });
        });
    });

    describe('DELETE /users/:id', () => {
        it('it should DELETE a user given the id', (done) => {
            User.create(attributes).then((user) => {
                chai.request(server)
                .delete('/users/' + user.id)
                .end((err, res) => {
                    res.should.have.status(204);
                    done();
                });
            });
        });
    });
});

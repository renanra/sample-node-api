process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();
const Post = require('../models').Post;

chai.use(chaiHttp);

describe('Posts', () => {
    let attributes = {
        title: "first blog post",
        content: "sample content",
    };

    beforeEach((done) => {
        Post.destroy({ truncate: { cascade: true } }).then(() => done());
    });

    describe('GET /posts', () => {
        it('it should GET all the posts', (done) => {
            chai.request(server)
            .get('/posts')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(0);
                done();
            });
        });
    });

    describe('POST /posts', () => {
        it('it should POST a post ', (done) => {
            chai.request(server)
            .post('/posts')
            .send(attributes)
            .end((err, res) => {
                res.should.have.status(201);
                res.body.should.be.a('object');
                res.body.should.have.property('title');
                res.body.should.have.property('content');
                done();
            });
        });
    });

    describe('GET /posts/:id', () => {
        it('it should GET a post by the given id', (done) => {
            Post.create(attributes).then((post) => {
                chai.request(server)
                .get('/posts/' + post.id)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('title');
                    res.body.should.have.property('content');
                    done();
                });
            }).catch((err) => { console.log(err) });

        });
    });

    describe('PUT /posts/:id', () => {
        it('it should UPDATE a post given the id', (done) => {
            Post.create(attributes).then((post) => {
                chai.request(server)
                .put('/posts/' + post.id)
                .send({ title: "updated title", content: "updated content" })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('title').eql('updated title');
                    res.body.should.have.property('content').eql('updated content');
                    done();
                });
            });
        });
    });

    describe('DELETE /posts/:id', () => {
        it('it should DELETE a post given the id', (done) => {
            Post.create(attributes).then((post) => {
                chai.request(server)
                .delete('/posts/' + post.id)
                .end((err, res) => {
                    res.should.have.status(204);
                    done();
                });
            });
        });
    });
});

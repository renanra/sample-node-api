process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();
const Reaction = require('../models').Reaction;
const User = require('../models').User;
const Post = require('../models').Post;

chai.use(chaiHttp);

describe('Reactions', () => {
    let userAttributes = {
        email: "user@example.com",
        password: "123456",
    };

    let postAttributes = {
        title: 'sample post',
        content: 'sample content'
    };

    let attributes = {
        name: 'like',
        PostId: null
    }

    before((done) => {
        User.create(userAttributes).then((user) => {
            Post.create(postAttributes).then((post) => {
                attributes.PostId = post.id;
                attributes.UserId = user.id;
                done();
            })
        });
    });

    beforeEach((done) => {
        Reaction.destroy({ truncate: { cascade: true } }).then(() => done());
    });

    describe('GET /reactions', () => {
        it('it should GET all the reactions', (done) => {
            chai.request(server)
            .get('/reactions')
            .auth(userAttributes.email, userAttributes.password)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(0);
                done();
            });
        });
    });

    describe('POST /reactions', () => {
        it('it should POST a reaction ', (done) => {
            chai.request(server)
            .post('/reactions')
            .auth(userAttributes.email, userAttributes.password)
            .send(attributes)
            .end((err, res) => {
                res.should.have.status(201);
                res.body.should.be.a('object');
                res.body.should.have.property('name');
                res.body.should.have.property('PostId');
                done();
            });
        });
    });

    describe('GET /reactions/:id', () => {
        it('it should GET a reaction by the given id', (done) => {
            Reaction.create(attributes).then((reaction) => {
                chai.request(server)
                .get('/reactions/' + reaction.id)
                .auth(userAttributes.email, userAttributes.password)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name');
                    res.body.should.have.property('PostId');
                    done();
                });
            }).catch((err) => { console.log(err) });

        });
    });

    describe('PUT /reactions/:id', () => {
        it('it should UPDATE a reaction given the id', (done) => {
            Reaction.create(attributes).then((reaction) => {
                chai.request(server)
                .put('/reactions/' + reaction.id)
                .send({ name: "dislike" })
                .auth(userAttributes.email, userAttributes.password)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('name').eql('dislike');
                    done();
                });
            });
        });
    });

    describe('DELETE /reactions/:id', () => {
        it('it should DELETE a reaction given the id', (done) => {
            Reaction.create(attributes).then((reaction) => {
                chai.request(server)
                .delete('/reactions/' + reaction.id)
                .auth(userAttributes.email, userAttributes.password)
                .end((err, res) => {
                    res.should.have.status(204);
                    done();
                });
            });
        });
    });
});

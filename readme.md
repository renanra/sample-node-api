# Sample node API


## Posts routes

```
GET /posts
GET /posts/:id

POST /posts
PUT /posts/:id
DELETE /posts/:id
```

Post entity looks like:

```
{
    title,
    content,
}
```
## Users routes

```
GET /users
GET /users/:id

POST /users
PUT /users/:id
DELETE /users/:id
```

User entity looks like:

```
{
    email,
    password,
}
```
## Reactions routes

```
GET /reactions
GET /reactions/:id

POST /reactions
PUT /reactions/:id
DELETE /reactions/:id
```

Reactions entity looks like:

```
{
    name: (like|dislike),
    PostID,
    UserID
}
```

UserID is set automatically using the current user.
Basic HTTP auth should be passed. The user's email should be used as username.

## Setup

```
npm install
node index.js
```

## Running the tests

```
node_modules/.bin/mocha
```

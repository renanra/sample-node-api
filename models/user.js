const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        email: {
            type: DataTypes.STRING,
            unique: true,
            validate: {
                notEmpty: true,
                isEmail: true
            }
        },
        password: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: true
            }
        }
    }, {});

    User.prototype.validPassword = function(password) {
        return bcrypt.compare(password, this.password);
    };

    User.associate = function(models) {
        User.hasMany(models.Reaction);
    };

    User.beforeCreate((user, options) => {
        return bcrypt.hash(user.password, 10)
        .then(hash => {
            user.password = hash;
        })
        .catch(err => {
            throw new Error();
        });
    });

    return User;
};

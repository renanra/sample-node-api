module.exports = (sequelize, DataTypes) => {
    const Post = sequelize.define('Post', {
        title: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: true
            }
        },
        content: {
            type: DataTypes.TEXT,
            validate: {
                notEmpty: true
            }
        }
    }, {});

    Post.associate = function(models) {
        Post.hasMany(models.Reaction);
    };

    return Post;
};

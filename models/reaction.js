module.exports = (sequelize, DataTypes) => {
    const Reaction = sequelize.define('Reaction', {
        name: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: true,
                isIn: [['like', 'dislike']]
            }
        },
        UserId: {
            type: DataTypes.INTEGER,
            validate: {
                notEmpty: true
            }
        },
        PostId: {
            type: DataTypes.INTEGER,
            validate: {
                notEmpty: true
            }
        }
    }, {});

    Reaction.associate = function(models) {
        Reaction.belongsTo(models.User);
        Reaction.belongsTo(models.Post);
    };

    return Reaction;
};

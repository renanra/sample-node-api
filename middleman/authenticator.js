const basicAuth = require('express-basic-auth')
const models = require('../models')
const bcrypt = require('bcrypt');

module.exports = [
    basicAuth({ authorizeAsync: true, authorizer: (username, password, cb) => {
        models.User.findOne({ where: { email: username } })
        .then((user) => {
            if (!user) {
                return cb(null, false);
            }

            return user.validPassword(password).then((res) => cb(null, res));
        })
        .catch((error) => { console.log(error); cb(null, false) });
    }}),
    (req, res, next) => {
        models.User.findOne({ where: { email: req.auth.user } })
        .then((user) => {
            res.locals.userId = user.id;
            req.currentUser = user;
            next();
        });
    }
];
